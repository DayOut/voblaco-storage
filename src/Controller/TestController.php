<?php

/**
 * Author: Andrej Belov <andrej.belov@murka.com>
 * Date: 29.04.2022
 */

namespace App\Controller;

use App\Document\TestObj;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class TestController extends AbstractController
{
    public function testEndpoint(DocumentManager $documentManager)
    {
        $response = [];
        $a = new TestObj();
        $a->setNick('teeest');
        $documentManager->persist($a);
        $documentManager->flush();

        return new JsonResponse($response);
    }
}
