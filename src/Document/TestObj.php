<?php

/**
 * Author: Andrej Belov <andrej.belov@murka.com>
 * Date: 29.04.2022
 */

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class TestObj
{
    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @var @MongoDB\Field(type="string")
     * @var string
     */
    public $nick;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return TestObj
     */
    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNick()
    {
        return $this->nick;
    }

    /**
     * @param string $nick
     * @return TestObj
     */
    public function setNick(string $nick)
    {
        $this->nick = $nick;
        return $this;
    }
}
